package codeinside.java;

import java.io.*;
import java.net.Socket;

public class Client {

    private final static String Ipaddr = "127.0.0.1";
    private final static Integer Port = 8000;

    public static void main(String[] args) throws IOException {
        Socket clientSocket = new Socket(Ipaddr, Port);
        System.out.println("Connection is successful");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        String clientMessage = consoleReader.readLine();

        writer.write(clientMessage);
        writer.newLine();
        writer.flush();

        String response = reader.readLine();
        System.out.println(response);

        writer.close();
        reader.close();
        clientSocket.close();
    }

}
