package codeinside.java;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Server {

    private final static Integer port = 8000;

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);

        System.out.println("Server started");

        Socket clientSocket = serverSocket.accept();
        System.out.println("Client accepted");

        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        String request = reader.readLine();

        Date date = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat(" E yyyy.MM.dd");

        String response = request + formatForDateNow.format(date);
        writer.write(response);
        writer.newLine();
        writer.flush();

        writer.close();
        reader.close();
        clientSocket.close();
        serverSocket.close();
    }

}
