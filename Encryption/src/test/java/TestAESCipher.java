import org.assertj.core.api.Assertions;
import org.junit.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class TestAESCipher {

    @Test
    public void testAES()
            throws NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException,
            BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException {

        String input = "Сообщение для шифрования AES";
        SecretKey key = AESCipher.generateKey(128);
        IvParameterSpec ivParameterSpec = AESCipher.generateIv();
        String algorithm = "AES/CBC/PKCS5Padding";
        String cipherText = AESCipher.encrypt(algorithm, input, key, ivParameterSpec);
        String plainText = AESCipher.decrypt(algorithm, cipherText, key, ivParameterSpec);
        Assertions.assertThat(input).isEqualTo(plainText);
    }

}
