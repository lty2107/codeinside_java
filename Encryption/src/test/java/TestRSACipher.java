import org.assertj.core.api.Assertions;
import org.junit.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

public class TestRSACipher {

    @Test
    public void testRSA() throws NoSuchAlgorithmException,
            NoSuchPaddingException, IllegalBlockSizeException,
            BadPaddingException, InvalidKeyException {

        String secretMessage = "Сообщение для шифрования RSA";
        KeyPair keyPair = RSACipher.generateKeyPair(2048);
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        String cipherText = RSACipher.encrypt("RSA", secretMessage, publicKey);
        String plainText = RSACipher.decrypt("RSA", cipherText, privateKey);
        Assertions.assertThat(secretMessage).isEqualTo(plainText);
    }

}
