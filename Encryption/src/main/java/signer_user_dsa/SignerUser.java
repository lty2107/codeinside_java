package signer_user_dsa;

import java.security.*;

public class SignerUser {
    private PublicKey publicKey;
    private PrivateKey privateKey;

    public SignerUser() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        SecureRandom secRan = new SecureRandom();
        keyPairGenerator.initialize(512, secRan);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        this.publicKey= keyPair.getPublic();
        this.privateKey = keyPair.getPrivate();
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

}
