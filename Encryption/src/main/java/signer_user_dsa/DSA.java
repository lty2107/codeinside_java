package signer_user_dsa;

import java.security.*;

public class DSA {

    public static void main(String[] args) {
        try{
            SignerUser signerUser = new SignerUser();
            String message = "Строка, полученная от пользователя";

            byte[] sign = signMessage(message.getBytes(), signerUser.getPrivateKey());

            PublicKey publicKey = signerUser.getPublicKey();

            System.out.println("Проверяем строку");
            validateMessageSignature(publicKey, message.getBytes(), sign);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void validateMessageSignature(PublicKey publicKey, byte[] message, byte[] signature) throws
            NoSuchAlgorithmException, InvalidKeyException, SignatureException {

        Signature clientSignature = Signature.getInstance("DSA");
        clientSignature.initVerify(publicKey);
        clientSignature.update(message);
        if (clientSignature.verify(signature)) {
            System.out.println("Цифровая подпись действительна");
        } else {
            System.err.println("Цифровая подпись недействительна");
        }
    }

    public static byte[] signMessage(byte[] message,PrivateKey privateKey) throws
            NoSuchAlgorithmException, InvalidKeyException, SignatureException {

        Signature sig = Signature.getInstance("DSA");
        sig.initSign(privateKey);
        sig.update(message);
        return sig.sign();
    }

}
