package chatClientProgram;

import java.io.*;
import java.net.Socket;

public class ReadThread extends Thread{
    private final BufferedReader reader;
    private final Socket socket;
    private final ChatClient chatClient;

    public ReadThread(Socket socket, ChatClient chatClient) throws IOException {
        this.socket = socket;
        this.chatClient = chatClient;

        InputStream inputStream = socket.getInputStream();
        reader = new BufferedReader(new InputStreamReader(inputStream));
    }

    public void run() {
        while (true){
            try {
                System.out.println("hello");
                String response = reader.readLine();
                System.out.println("\n" + response);
                if (chatClient.getUsername() != null) {
                    System.out.print(chatClient.getUsername() + " : ");
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
                break;
            }
        }
    }
}
