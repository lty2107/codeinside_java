package chatClientProgram;

import java.io.Console;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class WriteThread extends Thread{
    private final PrintWriter writer;
    private final Socket socket;
    private final ChatClient chatClient;

    public WriteThread(Socket socket, ChatClient chatClient) throws IOException {
        this.socket = socket;
        this.chatClient = chatClient;

        OutputStream outputStream = socket.getOutputStream();
        writer = new PrintWriter(outputStream, true);

    }

    public void run() {
        Console console = System.console();
        String username = console.readLine("Write your username: ");
        chatClient.setUsername(username);
        writer.println(username);

        String message;

        do{
            message = console.readLine("Write your message: ");
            writer.println(message);
        }
        while (!message.equals("Leave"));

        try {
            socket.close();
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
