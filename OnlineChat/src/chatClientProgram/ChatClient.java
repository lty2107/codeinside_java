package chatClientProgram;

import java.io.IOException;
import java.net.Socket;

public class ChatClient {
    private final Integer port;
    private final String hostname;
    private String username;

    public static void main(String[] args) {
        if (args.length < 2) return;

        Integer port = Integer.parseInt(args[0]);
        String hostname = args[1];

        ChatClient client = new ChatClient(port, hostname);
        try {
            client.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ChatClient(Integer port, String hostname){
        this.port = port;
        this.hostname = hostname;
    }

    public void start() throws IOException {
            Socket socket = new Socket(hostname, port);
            System.out.println("Connected to the chat server");
            new ReadThread(socket, this).start();
            new WriteThread(socket, this).start();
    }

    void setUsername(String username){
        this.username = username;
    }

    String getUsername(){
        return this.username;
    }

}
