package chatServerProgram;

import java.io.*;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class UserThread extends Thread {
    private Socket socket;
    private ChatServer chatServer;
    private PrintWriter writer;

    public UserThread(Socket socket, ChatServer chatServer){
        this.socket = socket;
        this.chatServer = chatServer;
    }

    public void run(){
        try {
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            writer = new PrintWriter(outputStream, true);
            
            printUsers();

            String username = bufferedReader.readLine();
            chatServer.addUser(username);

            chatServer.sendMessageToUsers(username + " landed on the server", this);

            String userMessage;

            do{
                userMessage = bufferedReader.readLine();
                String messageToChat = username + " : " + userMessage;
                chatServer.sendMessageToUsers(messageToChat, this);
            }
            while(!userMessage.equals("Leave"));

            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printUsers() {
        if (chatServer.hasUsers()){
            for(String user : chatServer.getUserNames()){
                System.out.println(user);
            }
        }
        else{
            System.out.println("No users connected");
        }
    }

    void sendMessage(String message){
        writer.println(message);
    }

}
