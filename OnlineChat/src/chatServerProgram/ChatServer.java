package chatServerProgram;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class ChatServer {
    private final Integer port;
    private final Set<String> userNames = new HashSet<>();
    private final Set<UserThread> userThreads = new HashSet<>();

    public ChatServer(int port){
        this.port = port;
    }

    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        ChatServer chatServer = new ChatServer(port);
        try {
            chatServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void start() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Server is running");
        while (true) {
            Socket socket = serverSocket.accept();
            UserThread userThread = new UserThread(socket, this);
            System.out.println("New USER");
            userThreads.add(userThread);
        }
    }

    void sendMessageToUsers(String message, UserThread sender){
        for (UserThread userName : userThreads){
            if (userName != sender){
                userName.sendMessage(message);
            }
        }
    }

    void addUser(String userName){
        userNames.add(userName);
    }

    Set<String> getUserNames(){
        return this.userNames;
    }

    boolean hasUsers(){
        return !this.userNames.isEmpty();
    }
}
